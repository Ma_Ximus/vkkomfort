//
//  VKOrangeButton.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 02.11.2020.
//

import UIKit

class VKOrangeButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentVerticalAlignment = ContentVerticalAlignment.center
        self.contentHorizontalAlignment = ContentHorizontalAlignment.center
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        self.layer.cornerRadius  = 4;
        self.backgroundColor     = #colorLiteral(red: 0.9921568627, green: 0.4470588235, blue: 0.2156862745, alpha: 1)
        self.setTitleColor(.white, for: .normal)
    }
   

}
