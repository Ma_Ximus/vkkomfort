//
//  VKLocalAuthVC.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

enum AuthMode {
    case signUp
    case signIn
}

class VKLocalAuthVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var signInPasswordField: UITextField!
    @IBOutlet weak var signUpPasswordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    var validSignIn = Observable<Bool>.just(false)
    var signInCode = BehaviorRelay(value: "")
    var validSignUp = Observable<Bool>.just(false)
    var signUpCode = BehaviorRelay(value: "")
    var confirmCode = BehaviorRelay(value: "")
    
    var enterDone = BehaviorRelay(value: false)
    
    var keychain: VKKeyChainPassItem!
    
    private let disposeBag = DisposeBag()
    
    private var authMode = VKBiometricAuth.isPasswordSet() ? AuthMode.signIn : AuthMode.signUp
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fields = [self.signInPasswordField,
                      self.signUpPasswordField,
                      self.confirmPasswordField]
        
        for field in fields {
            field?.tintColor = UIColor.gray.withAlphaComponent(0.3);
        }
        
        let isAuthComplete = keychain.isAuthComplete()
        
        if isAuthComplete {
            self.titleLabel?.text =  "Введите пароль приложения"
            self.signInPasswordField.placeholder = "пароль"
        } else {
            self.titleLabel?.text = "Установите пароль приложения"
        }
        
        self.signInPasswordField.isHidden = authMode == AuthMode.signUp
        self.signUpPasswordField.isHidden = authMode == AuthMode.signIn
        self.confirmPasswordField.isHidden = authMode == AuthMode.signIn
        
        self.validSignUp = Observable
            .combineLatest(self.signUpCode.asObservable(), self.confirmCode.asObservable()).filter({$0.0 != ""})
            .map({(password, confirm) -> Bool in
                print(password, (password == confirm), confirm)
                return (!password.isEmpty && !confirm.isEmpty) && (password == confirm)
            })
        
        self.validSignUp
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self]
                (valid) in
                guard let self = self else { return }
                print("validSignUp:", valid)
                if valid {
                    // Сохранение в связке ключей
                    do {
                        try self.keychain.savePassword(self.signUpCode.value)
                        print(VKBiometricAuth.isPasswordSet())
                        SVProgressHUD.showSuccess(withStatus: "Пароль установлен")
                        SVProgressHUD.dismiss(withDelay: 1.5)
                        self.keychain.setAuthComplete(true)
                    } catch  {
                        print(error)
                    }
                    self.dismiss(animated: true, completion: nil)
                } else if !self.confirmCode.value.isEmpty{
                    SVProgressHUD.showError(withStatus: "Пароли не совпадают. Попробуйте снова")
                    SVProgressHUD.dismiss(withDelay: 1.5)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.configureFields()
                    }
                }
            }).disposed(by: disposeBag)
        
        self.signInCode
            .subscribeOn(MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self]
                (valid) in
                guard let self = self else { return }
                guard !valid.isEmpty else {return}
                // Проверка на наличие и правильность пароля
                if let pass = try? self.keychain.readPassword(), pass.elementsEqual(valid) {
                    self.keychain.setAuthComplete(true)
                    self.enterDone.accept(true)
                } else {
                    SVProgressHUD.showError(withStatus: "Неправильный пароль. Попробуйте снова")
                    SVProgressHUD.dismiss(withDelay: 1.5)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.configureFields()
                    }
                }
            }).disposed(by: disposeBag)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureFields()
    }
    
    func configureFields() {
        if authMode == AuthMode.signIn {
            self.signInPasswordField.text = nil
            self.signInPasswordField.delegate = self
            self.signInPasswordField.becomeFirstResponder()
        } else {
            signUpCode.accept("")
            confirmCode.accept("")
            self.signUpPasswordField.text = nil
            self.confirmPasswordField.text = nil
            self.signUpPasswordField.delegate = self
            self.confirmPasswordField.delegate = self
            self.signUpPasswordField.becomeFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField ==  signUpPasswordField, let text = textField.text{
            signUpCode.accept(text)
            confirmPasswordField.becomeFirstResponder()
            return true
        }
        if textField ==  confirmPasswordField,  let text = textField.text{
            
            confirmCode.accept(text)
            print("confirmPasswordField: ", signInCode.value, "--", confirmCode.value)
            textField.resignFirstResponder()
            return true
        }
        if textField ==  signInPasswordField,  let text = textField.text{
            signInCode.accept(text)
            print("signInPasswordField: ", signInCode.value, "--", confirmCode.value)
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
}
