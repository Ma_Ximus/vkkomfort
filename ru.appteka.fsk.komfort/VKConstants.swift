//
//  VKConstants.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import Foundation

struct VKConstants {
    static let appService = "appPin"
    static let appAccount = "ru.appteka.fsk.komfort"
    static let keyMethod = "selecteMethod"
    static let kPasswordSet = "kPasswordSet"
}
