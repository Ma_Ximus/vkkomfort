//
//  VKStarter.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 30.10.2020.
//

import UIKit
import RxSwift
import RxCocoa

class VKStarter {
    
    func openEnterScreen(mainVC: UIViewController) {
        let enterVC = UIStoryboard.main.obtain(VKConfig.enterController)
        let navVC = UINavigationController(rootViewController: enterVC)
        navVC.modalPresentationStyle = .fullScreen
        navVC.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navVC.navigationBar.topItem?.title = ""
        mainVC.present(navVC, animated: false, completion: nil)
    }
}
