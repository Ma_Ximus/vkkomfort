//
//  VKEnterController.swift
//  ru.appteka.fsk.komfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

class VKEnterController: UIViewController {
    @IBOutlet weak var messegeLabel: UILabel!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private var keychain: VKKeyChainPassItem!
    private var biometric:IBiometricAuth!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function, self)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        keychain = VKKeyChainPassItem(service: VKConstants.appService, account: VKConstants.appAccount)
        
        biometric = VKConfig.biometricClass
        switch biometric.bioTipe {
        case .faceID(let title):
            self.firstButton.setTitle(title, for: .normal)
        case .touchID(let title):
            self.firstButton.setTitle(title, for: .normal)
        case .none:
            self.firstButton?.removeFromSuperview()
        }
        
        if firstButton != nil {
            configureBioButton()
        }
        
        keychain.authCompleteDone.bind {[weak self]  (complete) in
            let title = complete ? "PIN-code" : "Установить PIN-code"
            self?.secondButton.setTitle(title, for: .normal)
            if !complete {
                self?.openAuthVC(animated:false)
            }
        }.disposed(by: disposeBag)
        
        secondButton.rx.tap
            .do(onNext: { [weak self] (_) in
                self?.openAuthVC(animated:true)
            }).subscribe()
            .disposed(by: disposeBag)
    }
    
    func configureBioButton() {
        
        firstButton.rx
            .tap
            .observeOn(MainScheduler.instance)
            .flatMap({[weak self] (_) -> Observable<Bool> in
                        guard let selff = self else {return Observable.just(false)}
                        return selff.biometric.biometricAuthentication()
                            .subscribeOn(MainScheduler.instance)
                            .observeOn(MainScheduler.instance)
                            .catchError({ [weak selff] (error) -> Observable<Bool> in
                                if selff?.biometric.isUserFallbackError(error: error) ?? false{
                                    selff?.openAuthVC(animated:true)
                                } else if !(selff?.biometric.isUserCancelError(error: error) ?? false){
                                    selff?.showAlertMesage(code: (error as NSError).code)
                                }
                                return Observable.just(false)
                            })})
            .subscribe (onNext:{ [weak self] (result) in
                if result {
                    self?.dismiss(animated: true, completion:nil)
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    func openAuthVC(animated: Bool) {
        let authVC = UIStoryboard.main.obtain(VKConfig.localAuthController)
        authVC.keychain = self.keychain
        self.navigationController?.pushViewController(authVC, animated: animated)
        authVC.enterDone.subscribe {[weak self](done) in
            if done.element ?? false {
                authVC.dismiss(animated: false, completion: nil)
                self?.dismiss(animated: true, completion: nil)
            }
        }.disposed(by: disposeBag)
    }
    
    private func showAlertMesage(code: Int) {
        let message = biometric.biometricMessageForLA(errorCode: code)
        SVProgressHUD.showError(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 2.5)
    }
    
    deinit {
        print(#function, self)
    }
}

