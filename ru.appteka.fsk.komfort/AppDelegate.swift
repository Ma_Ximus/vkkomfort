//
//  AppDelegate.swift
//  ru.appteka.fsk.komfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import UIKit
import SVProgressHUD

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var needAuth = true
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window?.backgroundColor = #colorLiteral(red: 0.3830927014, green: 0.3831023574, blue: 0.3830972314, alpha: 1)
        SVProgressHUD.prepare()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if let mainVC = window?.rootViewController, needAuth {
            VKStarter().openEnterScreen(mainVC: mainVC)
            needAuth = false
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        needAuth = true
    }
    
}

