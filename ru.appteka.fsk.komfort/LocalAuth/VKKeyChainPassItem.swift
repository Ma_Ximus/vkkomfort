//
//  VKKeyChainPassItem.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 A struct for accessing generic password keychain items.
 */


import RxSwift
import RxCocoa

struct VKKeyChainPassItem {
    // MARK: Types
    
    enum KeychainError: Error {
        case noPassword
        case unexpectedPasswordData
        case unexpectedItemData
        case unhandledError(status: OSStatus)
    }
    
    var authCompleteDone = BehaviorRelay<Bool>(value: false)
    private let service: String
    private(set) var account: String
    private let accessGroup: String?
    
    // MARK: Intialization
    
    init(service: String, account: String, accessGroup: String? = nil) {
        self.service = service
        self.account = account
        self.accessGroup = accessGroup
        authCompleteDone.accept(isAuthComplete())
    }
    
    // MARK: Keychain access
    
    func readPassword() throws -> String  {
        /*
         Build a query to find the item that matches the service, account and
         access group.
         */
        print("readPassword(): 1")
        var query = VKKeyChainPassItem.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
        query[kSecMatchLimit as String] = kSecMatchLimitOne
        query[kSecReturnAttributes as String] = kCFBooleanTrue
        query[kSecReturnData as String] = kCFBooleanTrue
        
        // Try to fetch the existing keychain item that matches the query.
        var queryResult: AnyObject?
        let status = withUnsafeMutablePointer(to: &queryResult) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        print("readPassword(): 2", status)
        // Check the return status and throw an error if appropriate.
        guard status != errSecItemNotFound else { throw KeychainError.noPassword }
        guard status == noErr else { throw KeychainError.unhandledError(status: status) }
        
        // Parse the password string from the query result.
        guard let existingItem = queryResult as? [String : AnyObject],
              let passwordData = existingItem[kSecValueData as String] as? Data,
              let password = String(data: passwordData, encoding: String.Encoding.utf8)
        else {
            throw KeychainError.unexpectedPasswordData
        }
        print("readPassword(): 3", password)
        return password
    }
    
    func savePassword(_ password: String) throws {
        // Encode the password into an Data object.
        let encodedPassword = password.data(using: String.Encoding.utf8)!
        
        do {
            print("savePassword(): 1")
            // Check for an existing item in the keychain.
            try _ = readPassword()
            
            // Update the existing item with the new password.
            var attributesToUpdate = [String : AnyObject]()
            attributesToUpdate[kSecValueData as String] = encodedPassword as AnyObject?
            
            let query = VKKeyChainPassItem.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
            
            let status = SecItemUpdate(query as CFDictionary, attributesToUpdate as CFDictionary)
            
            // Throw an error if an unexpected status was returned.
            guard status == noErr else { throw KeychainError.unhandledError(status: status) }
            UserDefaults.standard.set(true, forKey: VKConstants.kPasswordSet)
        }
        catch KeychainError.noPassword {
            /*
             No password was found in the keychain. Create a dictionary to save
             as a new keychain item.
             */
            print("KeychainError.noPassword: 1")
            var newItem = VKKeyChainPassItem.keychainQuery(withService: service, account: account, accessGroup: accessGroup)
            newItem[kSecValueData as String] = encodedPassword as AnyObject?
            
            // Add a the new item to the keychain.
            let status = SecItemAdd(newItem as CFDictionary, nil)
            print("KeychainError.noPassword: 2")
            // Throw an error if an unexpected status was returned.
            guard status == noErr else { throw KeychainError.unhandledError(status: status) }
            UserDefaults.standard.set(true, forKey: VKConstants.kPasswordSet)
        }
    }
    
    private static func keychainQuery(withService service: String, account: String? = nil, accessGroup: String? = nil) -> [String : AnyObject] {
        var query = [String : AnyObject]()
        query[kSecClass as String] = kSecClassGenericPassword
        query[kSecAttrService as String] = service as AnyObject?
        
        if let account = account {
            query[kSecAttrAccount as String] = account as AnyObject?
        }
        
        if let accessGroup = accessGroup {
            query[kSecAttrAccessGroup as String] = accessGroup as AnyObject?
        }
        
        return query
    }
    
    func isAuthComplete() -> Bool {
        guard let set = UserDefaults.standard.object(forKey: self.service) as? Bool else {
            return false
        }
        return set;
    }
    func setAuthComplete(_ complete:Bool) {
        UserDefaults.standard.set(complete, forKey: self.service)
        authCompleteDone.accept(complete)
    }
}

