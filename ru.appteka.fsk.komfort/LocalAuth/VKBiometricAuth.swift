//
//  VKBiometricAuth.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import Foundation
import LocalAuthentication
import RxSwift
import RxCocoa

protocol IBiometricAuth: IBiometricAuthErrorHandler {
    var bioTipe:BiometricType {get}
    func biometricAuthentication() -> Observable<Bool>
    func setAppPass(pass: String) -> LAContext
}

protocol IBiometricAuthErrorHandler {
    func isUserFallbackError(error: Error) -> Bool
    func isUserCancelError(error: Error) -> Bool
    func biometricMessageForLA(errorCode: Int) -> String
}

enum BiometricType: Equatable {
    case none
    case touchID(String)
    case faceID(String)
}


class VKBiometricAuth: IBiometricAuth {
    
    var bioTipe:BiometricType {
        get {
            return biometricType()
        }
    }
    
    // Определение типа биометрической аутентификации для устройства и заголовки кнопок для экрана входа
    private func biometricType() -> BiometricType {
        
        let context = LAContext()
        
        if #available(iOS 11.0, *) {
            let _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil);
            switch context.biometryType {
            case .none:
                return .none
            case .touchID:
                return .touchID("Touch-ID")
            case .faceID:
                return .faceID("Face-ID")
            @unknown default:
                return .none
            }
        } else {
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
                return .touchID("Touch-ID")
            } else {
                return .none
            }
        }
        
    }
    
    func biometricAuthentication() -> Observable<Bool> {
        
        return Observable.create { (observer) -> Disposable in
            let context = LAContext()
            var authError: NSError?
            
            // Проверка возможности только биометрической аутентификации
            if context.canEvaluatePolicy (.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                
                let reasonString = .touchID("Touch-ID") == self.bioTipe ? "Приложите палец, чтобы войти" : "Посмотрите на экран, чтобы войти"
                
                // Заголовок для кнопки выбора альтернативы (для кнопки отмены оставляем системный)
                context.localizedFallbackTitle = "Ввести PIN"
                
                // Запрос и запуск биометрической аутентификации
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                    
                    if success {
                        
                        // Удачная аутентификация
                        
                        observer.onNext(true)
                    } else {
                        
                        guard let error = evaluateError else {
                            observer.onNext(false)
                            return
                        }
                        // Ошибка при неудачной аутентификации или выборе альтернативы
                        observer.onError(error)
                        
                    }
                    
                }
                
                
                
            } else {
                
                if let error = authError {
                    
                    // Ошибка при блокированной или не установленной биометрии
                    
                    observer.onError(error)
                }
            }
            return Disposables.create()
        }
        
    }
    
    func isUserFallbackError(error: Error) -> Bool {
        var result = false
        if (error as NSError).code == LAError.userFallback.rawValue {
            result = true
        }
        return result
    }
    
    func isUserCancelError(error: Error) -> Bool {
        var result = false
        if (error as NSError).code == LAError.userCancel.rawValue {
            result = true
        }
        return result
    }
    
    func biometricMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
        
        case LAError.authenticationFailed.rawValue:
            message = "Не предоставлены действительные данные"
            
        case LAError.appCancel.rawValue:
            message = "Прервано приложением"
            
        case LAError.invalidContext.rawValue:
            message = "Недопустимый контекст"
            
        case LAError.notInteractive.rawValue:
            message = "Не интерактивно"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Пароль не установлен на устройстве"
            
        case LAError.systemCancel.rawValue:
            message = "Аутентификация была отменена системой"
            
        case LAError.userCancel.rawValue:
            message = "Отменено пользователем"
            
        case LAError.userFallback.rawValue:
            message = "Пользователь решил использовать запасной вариант"
            
        default:
            message = biometricFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    
    func biometricFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        
        if #available(iOS 11.0, *) {
            switch errorCode {
            
            case LAError.biometryNotAvailable.rawValue:
                message = "Аутентификация не может быть запущена, поскольку устройство не поддерживает биометрическую аутентификацию."
                
            case LAError.biometryLockout.rawValue:
                message = "Заблокировано для биометрической аутентификации из-за слишком большого количества неудачных попыток аутентификации"
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Нет регистрации в биометрической аутентификации."
                
            default:
                message = "Неизвестная ошибка"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Слишком много неудачных попыток."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID недоступен на устройстве"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID не зарегистрирован на устройстве"
                
            default:
                message = "Неизвестная ошибка"
            }
        }
        
        return message;
    }
    
    func setAppPass(pass: String) -> LAContext {
        let localAuthenticationContext = LAContext.init()
        let theApplicationPassword = pass.data(using:String.Encoding.utf8)!
        localAuthenticationContext.setCredential(theApplicationPassword, type: LACredentialType.applicationPassword)
        return localAuthenticationContext
    }
    
    class func isPasswordSet() -> Bool {
        guard let set = UserDefaults.standard.object(forKey: VKConstants.kPasswordSet) as? Bool else {
            return false
        }
        return set;
    }
    
}

