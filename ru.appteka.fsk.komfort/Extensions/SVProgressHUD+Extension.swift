//
//  SVProgressHUD+Extension.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 02.11.2020.
//

import SVProgressHUD

extension SVProgressHUD {
    
    class func prepare() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setForegroundColor(.white)
        SVProgressHUD.setFont(UIFont.systemFont(ofSize: 13))
        SVProgressHUD.setBackgroundColor( #colorLiteral(red: 0.3097664118, green: 0.3098254502, blue: 0.3097627163, alpha: 1) )
        SVProgressHUD.setImageViewSize(CGSize(width: 20, height: 20))
        
    }
}
