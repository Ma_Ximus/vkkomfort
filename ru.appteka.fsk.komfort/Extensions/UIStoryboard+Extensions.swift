//
//  UIStoryboard+Extensions.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 29.10.2020.
//

import UIKit

extension UIStoryboard {
    static let main = UIStoryboard(name: "Main", bundle: nil)
    func obtain<T: UIViewController>(_ vcType: T.Type) -> T {
        guard let vc = instantiateViewController(withIdentifier: vcType.storyboardId) as? T else {
            preconditionFailure("Failed: \(vcType.storyboardId) for \(vcType) in \(self)")
        }
        return vc
    }
}

extension UIViewController {
    class var storyboardId: String {
        return String(describing: self.self)
    }
}
