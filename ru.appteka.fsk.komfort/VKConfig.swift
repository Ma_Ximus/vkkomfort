//
//  VKConfig.swift
//  VKKomfort
//
//  Created by Maxim Pakhotin on 01.11.2020.
//

import UIKit

struct VKConfig {
    
    static let enterController = VKEnterController.self
    static let localAuthController = VKLocalAuthVC.self
    
    static var biometricClass:IBiometricAuth {
        return VKBiometricAuth()
    }
}
